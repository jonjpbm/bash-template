#!/bin/bash
#meta data variables
__self__=$(basename $0)
__hostname__=$(hostname)
__scriptroot__=$(pwd)
__scriptTimeStamp__=$(date +%Y%m%d%H%M)
__bashVersion__=$(bash -version | grep 'version')


#functions
function show_help()
{
    echo -n -e "\nUsage: $__self__"
}

#check passed variables
if [ $# -eq 0 ]
then
        show_help
    exit 1
fi

#relevant script variables
